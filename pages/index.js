import React, { Component } from 'react';
import { Button, Input, Message } from 'semantic-ui-react';
import { Web3Contract, UportContract} from '../ethereum/inbox';
import { web3Client, web3Uport } from '../ethereum/web3';
import { tryP, chain } from 'fluture';

import { contract } from '../ethereum/build/address.json';

Array.prototype.first = function() {
    return this[0];
}

class Inbox extends Component {
    constructor() {
        super();

        this.state = {
            mnemonic: 'valid rack wish jelly outside mind truth useless edge fine bracket position',
            account: '',
            uportAccount: '',
            contract: {
                message: '',
                source: ''
            },
            data: {
                input: '',
                gasLimit: ''
            },
            loading: false,
            status: {
                timer: '0',
                transectionStatus: 'Pending',
                transactionHash: '',
                transactionRoot: 'https://rinkeby.etherscan.io/tx/'
            }
        };
    }

    initialState(message) {
        this.setState({
            contract: {
                message: message,
                source: web3Client.source
            },
            data: {
                input: message,
                gasLimit: '1000000'
            }
        });
    }
    initialAccounts(account) {
        this.setState({
            account: account
        });
    }
    initialUportAccount(account) {
        this.setState({
            uportAccount: account
        });
    }
    setLoadingStatus(status) {
        this.setState({
            loading: status
        });
    }
    setMnemonic(mnemonic) {
        this.setState({
            mnemonic: mnemonic
        });
    }
    setInput(message) {
        const gasLimit = this.state.data.gasLimit;
        this.setState({
            data: {
                input: message,
                gasLimit: gasLimit
            }
        });
    }
    setGasLimit(limit) {
        const input = this.state.data.input;
        this.setState({
            data: {
                input: input,
                gasLimit: limit
            }
        });
    }
    checkEmptyAccount(account) {
        return account === undefined || account === '' || account === null;
    }
    checkMetamask() {
        return this.state.contract.source === 'metamask';
    }
    componentDidMount() {
        if (web3Client.source === 'metamask') {
            const web3Contract = Web3Contract(contract.address, '');
            tryP(() => web3Contract.methods.getMessage().call())
                .fork(console.error, message => this.initialState(message));
            tryP(() => web3Client.utilities.eth.getAccounts())
                .fork(console.error, accounts => this.initialAccounts(accounts.first()));
        }
    }

    initialConnection(mnemonic) {
        const web3Contract = Web3Contract(contract.address, mnemonic);
        tryP(() => web3Client.utilities(mnemonic).eth.getAccounts())
            .fork(console.error, accounts => this.initialAccounts(accounts.first()));
        tryP(() => web3Contract.methods.getMessage().call())
            .fork(console.error, message => this.initialState(message));
    }

    uportConnection() {
        const uportClient = web3Uport();
        const uportContract = UportContract(contract.address);
        tryP(() => uportClient.utilities.eth.getAccounts())
            .fork(console.error, accounts => this.initialUportAccount(accounts.first()));
    }

    setMessage(message) {
        const source = this.state.contract.source;
        this.setState({
            contract: {
                message: message,
                source: source
            }
        });
        this.setLoadingStatus(false);
    }
    onReject() {
        console.log("Transaction Abort");
        this.setLoadingStatus(false);
    }

    metamaskSend(message) {
        const web3Contract = Web3Contract(contract.address, this.state.mnemonic);
        const account = this.state.account;
        let timer;
        tryP(() => {
                timer = this.startTimer();
                return web3Contract.methods.setMessage(message).send({
                    from: account
                });
            })
            .chain((data) => {
                this.updatetTansectionStatus("Succeed!", data.transactionHash);
                return tryP(() => web3Contract.methods.getMessage().call());
            })
            .fork(
                () => {
                    this.stopTimer(timer);
                    this.updatetTansectionStatus("Failed!", '');
                    this.onReject();
                },
                currentMessage => {
                    this.stopTimer(timer);
                    this.setMessage(currentMessage);
                });
    }

    customizedSend(message, gasLimit) {
        const web3Contract = Web3Contract(contract.address, this.state.mnemonic);
        const account = this.state.account;
        let timer;
        tryP(() => {
                timer = this.startTimer();
                return web3Contract.methods.setMessage(message).send({
                    gas: gasLimit,
                    from: account
                });
            })
            .chain((data) => {
                this.updatetTansectionStatus("Succeed!", data.transactionHash);
                return tryP(() => web3Contract.methods.getMessage().call());
            })
            .fork(
                (error) => {
                    this.stopTimer(timer);
                    this.updatetTansectionStatus("Failed!", '');
                    this.onReject();
                },
                currentMessage => {
                    this.stopTimer(timer);
                    this.setMessage(currentMessage);
                });
    }

    uportSend(message, gasLimit) {
        const uportContract = UportContract(contract.address);
        const account = this.state.uportAccount;
        tryP(() => uportContract.methods.setMessage(message).send({
                from: account
            }))
            .chain(() => tryP(() => uportContract.methods.getMessage().call()))
            .fork(() => this.onReject(), currentMessage => this.setMessage(currentMessage));
    }

    updateMessage(data) {
        this.setLoadingStatus(true);
        this.resetTransectionStatus();
        if (this.checkMetamask()) {
            this.metamaskSend(data.input);
        } else {
            this.customizedSend(data.input, data.gasLimit.toString());
        }
    }

    uportUpdateMessage(data) {
        this.setLoadingStatus(true);
        this.uportSend(data.input, data.gasLimit.toString());
    }

    startTimer() {
        return setInterval(() => {
            const status = this.state.status;
            status.timer = (parseFloat(status.timer) + 0.1).toFixed(2);
            this.setState({
                status: status
            });
        }, 100);
    }
    stopTimer(currentTimer) {
        clearInterval(currentTimer);
    }
    updatetTansectionStatus(signal, transactionHash) {
        const status = this.state.status;
        status.transectionStatus = signal;
        status.transactionHash = transactionHash;
        this.setState({
            status: status
        });
    }
    resetTransectionStatus() {
        this.setState({
            status: {
                timer: '0',
                transectionStatus: 'Pending',
                transactionHash: '',
                transactionRoot: 'https://rinkeby.etherscan.io/tx/'
            }
        });
    }
    switchTransectionColor(status) {
        switch (status) {
            case 'Succeed!':
                return 'green';
            case 'Failed!':
                return 'red';
            default:
                return 'black';
        }
    }
    
	render() {
		const statusIndicator = { 
			display: 'inline-block', 
			margin: '0 0 0 10px'
		};
		const resultIndicator = {
			...statusIndicator,
			color: this.switchTransectionColor(this.state.status.transectionStatus)
		}
		return(
			<div style={{ width: '100%' }}>
				<h2>Initial Account</h2>
			    <div style={{ width: '100%' }}>
				    <input style={{ display: 'inline-block', minWidth: '35em' }}
				        disabled={ this.checkMetamask() }
				    	value={this.state.mnemonic}
						onChange={event => this.setMnemonic(event.target.value)}
			        />
			        <Button 
			        	style={{ display: 'inline-block' }}
			        	disabled={ this.checkMetamask() }
			        	onClick={this.initialConnection.bind(this, this.state.mnemonic)}
			        	primary>
		            	Initialize Account
	          		</Button>
	          		
			    </div>
			    <h2>Metamask & Integrated Connection</h2>
				<p> Current Account is: 
					{ this.checkEmptyAccount(this.state.account) ? 
						this.checkMetamask() ? " Please Unlock Metamask!" : " Please Initialize Account" : 
						" " + this.state.account 
					}
				</p>
				
				<input style={{ display: 'inline-block' }}
					value={this.state.data.input}
					disabled = { this.checkEmptyAccount(this.state.account) }
					onChange={event => this.setInput(event.target.value)}
		        />
		        <input style={{ display: 'inline-block' }}
					value={this.state.data.gasLimit}
					disabled = { this.checkMetamask() || this.checkEmptyAccount(this.state.account) }
					onChange={event => this.setGasLimit(event.target.value)}
		        />
		        <Button 
		        	style={{ display: 'inline-block' }}
		        	onClick={this.updateMessage.bind(this, this.state.data)}
		        	disabled = { this.state.loading || this.checkEmptyAccount(this.state.account) }
		        	primary>
	            	{ this.state.loading ? "Loading" : "Set" }
	          	</Button>
	          	<p style={statusIndicator}>Timer: {this.state.status.timer}s</p>
	          	{ this.state.status.timer === '0' ? null :
		          	<div style={{ display: 'inline-block' }}>
		          		<p style={resultIndicator}>{this.state.status.transectionStatus}</p>
		          		{	this.state.status.transactionHash === '' ? null :
		          			<a  style={statusIndicator} 
		          				href={this.state.status.transactionRoot + this.state.status.transactionHash}>
		          			Transaction Link
		          			</a>
		          		}
		          	</div>
		        }
	        <p>Current Message: { this.state.contract.message }</p>
	        <h2>Uport Connection</h2>
	        <p> Current Uport Account is: {this.state.uportAccount} </p>
	        <Button 
	        	style={{ display: 'inline-block' }}
	        	onClick={this.uportConnection.bind(this)}
	        	primary>
            	Uport Authenticate
      		</Button>
      		<Button 
	        	style={{ display: 'inline-block' }}
	        	onClick={this.uportUpdateMessage.bind(this, this.state.data)}
	        	disabled = { this.state.loading || this.checkEmptyAccount(this.state.uportAccount) }
	        	primary>
            	{ this.state.loading ? "Loading" : "Uport!" }
          	</Button>
          	</div>
	    );
	}
}


export default Inbox
