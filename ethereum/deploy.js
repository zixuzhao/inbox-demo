const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const fs = require('fs-extra');

const inboxContract = require('./build/Inbox.json');

const provider = new HDWalletProvider(
  'valid rack wish jelly outside mind truth useless edge fine bracket position',
  'https://rinkeby.infura.io/v3/8d1ba1e9ef484906ba94d560cc1d3a87'
);
const web3 = new Web3(provider);

const deploy = async () => {
  const accounts = await web3.eth.getAccounts();

  console.log('Attempting to deploy from account', accounts[0]);

  const result = await new web3.eth.Contract(
    JSON.parse(inboxContract.interface)
  )
    .deploy({ data: inboxContract.bytecode, arguments: ['Hi there!'] })
    .send({ gas: '1000000', from: accounts[0] });

  fs.writeFile(__dirname + '/build/address.json', 
    JSON.stringify({ contract: { address: result.options.address }}));  
  console.log('Contract deployed to', result.options.address);
};
deploy();