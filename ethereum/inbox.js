import { web3Client, web3Uport } from './web3';
import InboxContract from './build/Inbox.json';

export const Web3Contract = (address, mnemonic) => {
	if (web3Client.source === 'metamask') {
  		return new web3Client.utilities.eth.Contract(JSON.parse(InboxContract.interface), address);
  	} else {
  		const utilities = web3Client.utilities(mnemonic);
  		return new utilities.eth.Contract(JSON.parse(InboxContract.interface), address);
  	}
};

export const UportContract = (address) => {
	const uportClient = web3Uport();
	return new uportClient.utilities.eth.Contract(JSON.parse(InboxContract.interface), address);
}
