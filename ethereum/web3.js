import Web3 from 'web3';
import HDWalletProvider from 'truffle-hdwallet-provider';
import { Connect } from 'uport-connect';

let web3;

if (typeof window !== 'undefined' && typeof window.web3 !== 'undefined') {
  // We are in the browser and metamask is running.
  web3 = { source: 'metamask', utilities: new Web3(window.web3.currentProvider) };
} else {
  // We are on the server *OR* the user is not running metamask
  const provider = (mnemonic) => new HDWalletProvider(
  	mnemonic, 'https://rinkeby.infura.io/v3/8d1ba1e9ef484906ba94d560cc1d3a87');
  web3 = { source: 'customer', utilities: (mnemonic) => new Web3(provider(mnemonic)) };
}


export const web3Client = web3;
export const web3Uport = () => {
	const connect = new Connect('meri-demo', {
		clientId: '2p1Qf5dm31eV8FEVDKqVwftR9LBdUYRNYcW',
		network: 'rinkeby',
		provider: 'https://rinkeby.infura.io/v3/8d1ba1e9ef484906ba94d560cc1d3a87'
	});
	const uportProvider = connect.getProvider();
	return { source: 'uport', connect: connect, utilities: new Web3(uportProvider) };
}